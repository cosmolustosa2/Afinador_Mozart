import kivy
from kivy.app import App
from kivy.app import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition
from kivy.lang import Builder
from kivy.core.window import Window #usada para chamar a funcao clearcolor
from kivy.utils import get_color_from_hex

Window.clearcolor = get_color_from_hex("#00FF00")


class PaginaInicial(Screen):
    pass


class Treinador(Screen):
    pass


class Gerenciador(ScreenManager):
    def escolheTreinador(self):
        self.current = 'treinador'

    def escolhepaginaInicial(self):
        self.current = 'paginaInicial'


class InterfaceApp(App):
    def build(self):
        self.root = Gerenciador()
        return self.root


if __name__ == '__main__':
    InterfaceApp().run()